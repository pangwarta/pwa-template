const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const { ANALYZE } = process.env
const withTypescript = require('@zeit/next-typescript')
const withOffline = require('next-offline')
// const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

module.exports = withTypescript(withOffline({
  webpack: function (config, { isServer }) {
    if (ANALYZE) {
      config.plugins.push(new BundleAnalyzerPlugin({
        analyzerMode: 'server',
        analyzerPort: isServer ? 8888 : 8889,
        openAnalyzer: true
      }))
    }
    // Do not run type checking twice:
    // if (isServer) config.plugins.push(new ForkTsCheckerWebpackPlugin())
    return config
  }
}))
