import { ActionType, getType } from 'typesafe-actions'
import * as actions from './actions'

export type RootAction = ActionType<typeof actions> 

export interface RootState {
  readonly count: number,
  readonly error: Error | boolean,
  readonly lastUpdate: number,
  readonly light: boolean,
  readonly placeholderData?: actions.UserJson.User[]
}

export const exampleInitialState: RootState = {
  count: 0,
  error: false,
  lastUpdate: 0,
  light: false
}

export default (state = exampleInitialState, action: RootAction) => {
  switch (action.type) {
    case getType(actions.failure):
      return {
        ...state,
        ...{error: action.payload}
      }

    case getType(actions.increment):
      return {
        ...state,
        ...{count: state.count + 1}
      }

    case getType(actions.decrement):
      return {
        ...state,
        ...{count: state.count - 1}
      }

    case getType(actions.reset):
      return {
        ...state,
        ...{count: exampleInitialState.count}
      }

    case getType(actions.loadDataSuccess):
      return {
        ...state,
        ...{placeholderData: action.payload}
      }

    case getType(actions.tickClock):
      return {
        ...state,
        ...{lastUpdate: action.payload.ts, light: !!action.payload.light}
      }

    default:
      return state
  }
}
