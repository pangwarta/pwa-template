import {createStore, applyMiddleware, Middleware, GenericStoreEnhancer, compose, Store} from 'redux'
import createSagaMiddleware, { Task } from 'redux-saga'

import rootReducer, {exampleInitialState, RootState} from './reducer'
import rootSaga from './saga'

const sagaMiddleware = createSagaMiddleware()

type ComposeWithDevTools = { composeWithDevTools: typeof compose }

const bindMiddleware: (middleware: Middleware[]) => GenericStoreEnhancer = (middleware) => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools }: ComposeWithDevTools = require('redux-devtools-extension')
    return composeWithDevTools(applyMiddleware(...middleware))
  }
  return applyMiddleware(...middleware)
}

interface SagaStore extends Store<RootState> {
  runSagaTask?: () => void
  sagaTask?: Task
}

function configureStore (initialState = exampleInitialState): Store<RootState> {
  const store: SagaStore = createStore(
    rootReducer,
    initialState,
    bindMiddleware([sagaMiddleware])
  )

  store.runSagaTask = () => {
    store.sagaTask = sagaMiddleware.run(rootSaga)
  }

  store.runSagaTask()
  return store
}

export default configureStore
