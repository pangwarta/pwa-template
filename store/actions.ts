import { createAction } from 'typesafe-actions'

// TODO: How TF do we make saga calls type-safe while allowing them to be used
// with typesafe-actions? Lines 5-25 are pretty gross and hacky, yuck!
// Is there a better way???
interface ActionTypes {
  FAILURE: 'FAILURE',
  INCREMENT: 'INCREMENT',
  DECREMENT: 'DECREMENT',
  RESET: 'RESET',
  LOAD_DATA: 'LOAD_DATA',
  LOAD_DATA_SUCCESS: 'LOAD_DATA_SUCCESS',
  START_CLOCK: 'START_CLOCK',
  TICK_CLOCK: 'TICK_CLOCK'
}

export const actionTypes: ActionTypes = {
  FAILURE: 'FAILURE',
  INCREMENT: 'INCREMENT',
  DECREMENT: 'DECREMENT',
  RESET: 'RESET',
  LOAD_DATA: 'LOAD_DATA',
  LOAD_DATA_SUCCESS: 'LOAD_DATA_SUCCESS',
  START_CLOCK: 'START_CLOCK',
  TICK_CLOCK: 'TICK_CLOCK'
}

export const failure = createAction(actionTypes.FAILURE, resolve => {
  return (error: Error) => resolve(error)
})

export const increment = createAction(actionTypes.INCREMENT)

export const decrement = createAction(actionTypes.DECREMENT)

export const reset = createAction(actionTypes.RESET)

export const loadData = createAction(actionTypes.LOAD_DATA)

export const loadDataSuccess = createAction(actionTypes.LOAD_DATA_SUCCESS, resolve => {
  return (data: UserJson.User[]) => resolve(data)
})

export declare module UserJson {

  export interface Geo {
      lat: string;
      lng: string;
  }

  export interface Address {
      street: string;
      suite: string;
      city: string;
      zipcode: string;
      geo: Geo;
  }

  export interface Company {
      name: string;
      catchPhrase: string;
      bs: string;
  }

  export interface User {
      id: number;
      name: string;
      username: string;
      email: string;
      address: Address;
      phone: string;
      website: string;
      company: Company;
  }
}

export const startClock = createAction(actionTypes.START_CLOCK)

export const tickClock = createAction(actionTypes.TICK_CLOCK, resolve => {
  return (isServer: boolean) => resolve({ light: !isServer, ts: Date.now() })
})
