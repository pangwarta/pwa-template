import React, {Component, MouseEvent} from 'react'
import {connect, Dispatch} from 'react-redux'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import {increment, decrement, reset} from '../store/actions'

interface Props {
  count: number
  dispatch: Dispatch
}

export interface SFCSpreadAttributesProps {
  className?: string;
  style?: React.CSSProperties;
}

export const SFCSpreadAttributes: React.SFC<SFCSpreadAttributesProps> = (props) => {
  const { children, ...restProps } = props;

  return (
    <div {...restProps}>
      {children}
    </div>
  );
};

class Counter extends Component<Props> {

  increment = (_e: MouseEvent<HTMLButtonElement>): void => {
    this.props.dispatch(increment())
  }

  decrement = (_e: MouseEvent<HTMLButtonElement>): void => {
    this.props.dispatch(decrement())
  }

  reset = (_e: MouseEvent<HTMLButtonElement>): void => {
    this.props.dispatch(reset())
  }

  render () {
    const {count} = this.props
    const style: React.CSSProperties = {
      padding: '0 0 20px 0'
    }

    return (
      <div style={style}>
        <Typography variant="subheading" gutterBottom>
          Count: {count}
        </Typography>
        <Button size="small" mini variant="outlined" onClick={this.increment}>+1</Button>
        <Button size="small" mini variant="outlined" onClick={this.decrement}>-1</Button>
        <Button size="small" mini variant="outlined" onClick={this.reset}>Reset</Button>
      </div>
    )
  }
}

const mapStateToProps = ({count}: { count: number }) => ({count})
export default connect(mapStateToProps)(Counter)
