import React from 'react'
import { withStyles } from '@material-ui/core'
import { StyleRules } from '@material-ui/core/styles'

const pad = (n: number) => (n < 10 ? `0${n}` : n)

const format = (t: Date) => {
  const hours = t.getUTCHours()
  const minutes = t.getUTCMinutes()
  const seconds = t.getUTCSeconds()
  return `${pad(hours)}:${pad(minutes)}:${pad(seconds)}`
}

const styles: () => StyleRules = () => ({
  root: {
    padding: '15px',
    display: 'inline-block',
    color: '#82FA58',
    font: '50px menlo, monaco, monospace',
    backgroundColor: '#000'
  },
  light: {
    backgroundColor: '#999'
  }
})

export interface Props {
  lastUpdate: number,
  light: boolean,
  classes: {
    root: string,
    light: string
  }
}

const Clock: React.SFC<Props> = ({ lastUpdate, light, classes }) => {
  return (
    <div className={`${classes.root} ${light ? classes.light : ''}`}>
      {format(new Date(lastUpdate))}
    </div>
  )
}

export default withStyles(styles)(Clock)
