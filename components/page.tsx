import Link from 'next/link'
import {connect} from 'react-redux'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import { withStyles, Theme, StyleRules } from '@material-ui/core/styles';

import Counter from './counter'
import Clock from './clock'
import withRoot from '../src/withRoot'
import { UserJson } from '../store/actions'

const styles: (theme: Theme) => StyleRules = (_theme: Theme) => ({
  root: {
    margin: '20px'
  }
});

export interface Props {
  error: Error,
  lastUpdate: number,
  light: boolean,
  linkTo: string,
  NavigateTo: string,
  placeholderData?: UserJson.User[],
  title: string,
  classes: { root: string }
}

const Page: React.SFC<Props> = (props: Props) => {
  const {error, lastUpdate, light, linkTo, NavigateTo, placeholderData, title, classes} = props
  return (
    <div className={classes.root}>
      <Typography gutterBottom variant="title" >{title}</Typography>
      <Clock lastUpdate={lastUpdate} light={light} />
      <Counter />
      <nav>
        <Link href={linkTo} prefetch>
          <a><Typography variant="body1">Navigate: {NavigateTo}</Typography></a>
        </Link>
      </nav>
      <Paper>
        {placeholderData &&
          <pre>
            <code>
              {JSON.stringify(placeholderData, null, 2)}
            </code>
          </pre>}
        {error &&
          <p style={{color: 'red'}}>
            Error: {error.message}
          </p>}
      </Paper>
    </div>
  )
}

export default connect(state => state)(withRoot(withStyles(styles)(Page)))
