import React from 'react'
import {connect, Dispatch} from 'react-redux'

import {loadData, startClock, tickClock} from '../store/actions'
import Page from '../components/page'
import { Store } from 'redux';
import { RootState } from '../store/reducer';

export interface IndexProps {
  ctx: {
    store: Store<RootState>,
    isServer: boolean
  },
  dispatch: Dispatch
}

class Index extends React.Component<IndexProps> {
  static async getInitialProps (props: IndexProps) {
    const { store, isServer } = props.ctx
    store.dispatch(tickClock(isServer))
    return { isServer }
  }

  componentDidMount () {
    this.props.dispatch(startClock())
    this.props.dispatch(loadData())
  }

  render () {
    return <Page title='Index Page' linkTo='/other' NavigateTo='Other Page' />
  }
}

export default connect()(Index)
